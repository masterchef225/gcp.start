terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {

  credentials = file("GCP_key.json")

  project = "halo-start-1"
  region  = "europe-west3"
  zone    = "europe-west3-c"
}

resource "google_compute_network" "vpc" {
 name                    = "test-vpc"
 auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet-pr" {
 name          = "test-subnet-pr"
 ip_cidr_range = "192.168.1.0/24"
 network       = "test-vpc"
 depends_on    = ["google_compute_network.vpc"]
 region        = "europe-west3"
}

resource "google_compute_router" "router" {
  name    = "my-router"
  //region  = google_compute_subnetwork.subnet.region
  network = "test-vpc"
}

resource "google_compute_router_nat" "nat_manual" {
  name   = "my-router-nat"
  router = "my-router"
  

  nat_ip_allocate_option = "AUTO_ONLY"

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = "test-subnet-pr"
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

resource "google_compute_subnetwork" "subnet-pu" {
 name          = "test-subnet-pu"
 ip_cidr_range = "192.168.2.0/24"
 network       = "test-vpc"
 depends_on    = ["google_compute_network.vpc"]
 region        = "europe-west3"
}

resource "google_compute_firewall" "firewall" {
  name    = "test-firewall"
  network = "test-vpc"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "instance-pr" {
  name         = "test-pr"
  machine_type = "e2-medium"
  zone         = "europe-west3-c"
  depends_on   = ["google_compute_subnetwork.subnet-pr"]
  tags = ["instance", "private"]
network_interface {
    network = "test-vpc"
    subnetwork = "test-subnet-pr"
}
  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }
}


resource "google_compute_instance" "instance-nat" {
  name         = "test-nat"
  machine_type = "e2-medium"
  zone         = "europe-west3-c"
  depends_on   = ["google_compute_subnetwork.subnet-pr"]
  tags = ["instance", "private"]
network_interface {
    network = "test-vpc"
    subnetwork = "test-subnet-pr"
    access_config {}

}
  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }
}

resource "google_compute_address" "static" {
  name = "ipv4-address"
}

resource "google_compute_instance" "instance-pu" {
  name         = "test-pu"
  machine_type = "e2-medium"
  zone         = "europe-west3-c"
  depends_on   = ["google_compute_subnetwork.subnet-pu"]
  tags = ["instance", "public"]
network_interface {
    network = "test-vpc"
    subnetwork = "test-subnet-pu"
    access_config {
      nat_ip = google_compute_address.static.address
    }
   
}
  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }
  metadata_startup_script = "echo hi > /test.txt"
}

resource "google_container_cluster" "primary" {
  name     = "clustertest"
  location = "europe-west3"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = "europe-west3"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"

  }
}

